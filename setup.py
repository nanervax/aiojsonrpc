from os import path
from setuptools import setup, find_packages

from aiojsonrpc import version


setup(
    name='aiojsonrpc',
    version=version,
    packages=find_packages(),
    long_description=open(path.join(path.dirname(__file__), 'README.md')).read(),
    python_requires='>=3.7',
    install_requires=[
        'fastjsonschema<3.0',
        'simplejson<4.0',
        'pytest',
    ],
)
