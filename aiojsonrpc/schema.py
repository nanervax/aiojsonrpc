import fastjsonschema

from fastjsonschema import JsonSchemaException

request_validator = fastjsonschema.compile({
    'type': 'object',
    'properties': {
        'jsonrpc': {'type': 'string', 'const': '2.0'},
        'method': {'type': 'string'},
        'params': {'type': ['array', 'object']},
        'id': {'type': ['number', 'string', 'null']}
    },
    'required': ['jsonrpc', 'method'],
    'additionalProperties': False
})

response_validator = fastjsonschema.compile({
    'type': 'object',
    'properties': {
        'jsonrpc': {'type': 'string', 'const': '2.0'},
        'result': {},
        'error': {
            'type': 'object',
            'properties': {
                'code': {'type': 'number'},
                'message': {'type': 'string'},
                'data': {}
            },
            'required': ['code', 'message'],
            'additionalProperties': False
        },
        'id': {'type': ['number', 'string', 'null']}
    },
    'required': ['jsonrpc', 'id'],
    'oneOf': [
        {'required': ['result']},
        {'required': ['error']},
    ],
    'additionalProperties': False
})


def _validate(validator, rpc_obj):
    try:
        validator(rpc_obj)
    except JsonSchemaException:
        return False
    else:
        return True


def validate_response(rpc_obj):
    return _validate(response_validator, rpc_obj)


def validate_request(rpc_obj):
    return _validate(request_validator, rpc_obj)
