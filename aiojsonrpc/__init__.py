from .endpoint import Endpoint

__version = (0, 0, 5)
__version__ = version = '.'.join(map(str, __version))

__all__ = ['Endpoint']
