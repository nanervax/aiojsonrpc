import asyncio
from collections import namedtuple
from concurrent.futures import ThreadPoolExecutor
from abc import abstractmethod

from .constants import *


class BaseMethodExecutor:
    """
    Исполнитель массового запроса
    Уведомления просто исполняет
    """
    def __init__(self, dispatch_func, event_loop):
        self.dispatch_func = dispatch_func
        self.event_loop = event_loop

    @abstractmethod
    async def collect_results(self, batch_request):
        raise NotImplemented

    async def handle_batch(self, batch_request):
        results = await self.collect_results(batch_request)

        # На уведомления отвечать не нужно
        return list(filter(lambda result: result is not None, results))


class AsyncExecutor(BaseMethodExecutor):
    """
    Асинхронный исполнитель массового запроса
    CPU bound задачи им лучше не исполнять
    """
    async def collect_results(self, batch_request):
        async_tasks = [
            asyncio.create_task(self.dispatch_func(request))
            for request in batch_request
        ]

        return await asyncio.gather(
            *async_tasks,
            loop=self.event_loop,
            return_exceptions=True
        )


class ThreadExecutor(BaseMethodExecutor):
    """
    Исполнитель на основе потоков, асинхронно ждет результата из пула потоков
    """
    # На данном этапе пул один
    pool_executor = ThreadPoolExecutor()

    async def collect_results(self, batch_request):
        return await asyncio.gather(*[
            self.event_loop.run_in_executor(
                self.pool_executor,
                self.dispatch_func,
                request
            )
            for request in batch_request
        ])

    async def collect_one_result(self, request):
        return await self.event_loop.run_in_executor(
            self.pool_executor,
            self.dispatch_func,
            request
        )


class CompositeExecutor(BaseMethodExecutor):
    """
    Одновременно нужно выполнять и асинхронные и CPU bound задачи,
    причем ждать результат выполнения асинхронно
    ProcessExecutor не сделан осознанно, это сильно усложняет систему +
    будут проблемы с масштабированием, это должно решаться на другом уровне
    """
    ExecutorTasks = namedtuple('ExecutorTasks', ('executor', 'tasks'))

    def __init__(self, method_types, dispatch_func_sync, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.method_types = method_types
        self.async_executor = AsyncExecutor(self.dispatch_func, self.event_loop)
        self.thread_executor = ThreadExecutor(dispatch_func_sync, self.event_loop)

    async def collect_results(self, batch_request):
        request_executors = {
            MARK_ASYNC: self.ExecutorTasks(self.async_executor, []),
            MARK_THREAD: self.ExecutorTasks(self.thread_executor, []),
        }

        # На данном этапе нет никакой валидации, поэтому неизвестно какая
        # структура внутри списка, если что-то не распозналось, то логично
        # обработать и отдать в async (будет просто ошибка)
        for request in batch_request:
            if not isinstance(request, dict) or not request.get('method'):
                request_executors[MARK_ASYNC].tasks.append(request)

            req_method = request['method']
            if req_method not in self.method_types:
                request_executors[MARK_ASYNC].tasks.append(request)
                continue

            req_method_type = self.method_types[req_method]
            request_executors[req_method_type].tasks.append(request)

        results = await asyncio.gather(
            *[
                executor_tasks.executor.collect_results(executor_tasks.tasks)
                for executor_type, executor_tasks in request_executors.items() if
                executor_tasks.tasks
            ],
            loop=self.event_loop,
            return_exceptions=True
        )

        # Перед возвратом нужно "расплющить" то что собрано
        return [result for sublist in results for result in sublist]
