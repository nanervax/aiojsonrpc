from functools import partial
from .response import Response

# Ошибки, определенные протоколом, описание на
# https://www.jsonrpc.org/specification
PARSE_ERROR = -32700
INVALID_REQUEST = -32600
METHOD_NOT_FOUND = -32601
INVALID_PARAMS = -32602
INTERNAL_ERROR = -32603

# Ошибки, созданные не протоколом, так как протокол не знает что делать
# в некоторых ситуациях или делает это неопределенно

# Пришел невалидный JSON в ответе
RESPONSE_PARSE_ERROR = -31000
# Пришел невалидный ответ
INVALID_RESPONSE = -31001
# Пришел невалидный batch ответ
INVALID_RESPONSE_BATCH = -31002

# Подготовленные ф-ции для генерирования ошибок
parse_error = partial(Response.make_error, id=None, err_code=PARSE_ERROR, err_msg='Parse error')
invalid_request = partial(Response.make_error, id=None, err_code=INVALID_REQUEST, err_msg='Invalid Request')
method_not_found = partial(Response.make_error, err_code=METHOD_NOT_FOUND, err_msg='Method not found')
invalid_params = partial(Response.make_error, err_code=INVALID_PARAMS, err_msg='Invalid Params')
internal_error = partial(Response.make_error, err_code=INTERNAL_ERROR, err_msg='Internal error')

response_parse_error = partial(Response.make_error, id=None, err_code=RESPONSE_PARSE_ERROR, err_msg='Response parse error')
invalid_response = partial(Response.make_error, id=None, err_code=INVALID_RESPONSE, err_msg='Invalid Response')
invalid_response_batch = partial(Response.make_error, id=None, err_code=INVALID_RESPONSE_BATCH, err_msg='Invalid batch Response')
