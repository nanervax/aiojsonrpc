class Response:
    """
    Ответа по протоколу jsonrpc 2.0
    Так же позволяет формировать ошибку или результат
    """
    @classmethod
    def _make_response(cls, id, result=None, error=None):
        response = {
            'jsonrpc': '2.0',
            'id': id
        }
        if result is not None:
            response['result'] = result
        if error is not None:
            response['error'] = error
        return response

    @classmethod
    def _make_error(cls, code, message, data=None):
        error = {
            'code': code,
            'message': message,
        }

        if data:
            error['data'] = data
        return error

    @classmethod
    def make_result(cls, id, result):
        return cls._make_response(id, result=result)

    @classmethod
    def make_error(cls, id, err_code, err_msg, err_data=None):
        error = cls._make_error(err_code, err_msg, err_data)
        return cls._make_response(id, error=error)

    def __init__(self, dict_repr):
        self.version = dict_repr['jsonrpc']
        self.id = dict_repr['id']
        result = dict_repr.get('result')
        error = dict_repr.get('error')

        if error:
            self.error = error

        elif result:
            self.result = result
        else:
            raise AttributeError(
                '"result" or "error" field should be represented in "dict_repr"'
            )
