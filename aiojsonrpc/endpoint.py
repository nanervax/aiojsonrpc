import asyncio
import traceback

import simplejson
from simplejson import JSONDecodeError

from .helper import is_invalid_params
from .defined_errors import parse_error, invalid_request, method_not_found, \
    invalid_params, internal_error
from .schema import validate_request
from .exceptions import RpcResponseException, RpcException
from .response import Response
from .exec import CompositeExecutor, ThreadExecutor
from .constants import *


class Endpoint:
    """
    Основной класс пакета
    Позволяет регистрировать внутреннюю маршрутизацию
    Имеет версионность, отражается в endpoint
    """
    def __init__(self, namespace='jsonrpc', version='v1', event_loop=None):
        self.method_router = {}
        self.method_types = {}
        self.namespace = namespace
        self.version = version
        self.batch_executor = CompositeExecutor(
            dispatch_func=self._prepare_response,
            dispatch_func_sync=self._prepare_response_sync,
            event_loop=event_loop or asyncio.get_event_loop(),
            method_types=self.method_types
        )
        self.thread_executor = ThreadExecutor(
            dispatch_func=self._prepare_response_sync,
            event_loop=event_loop or asyncio.get_event_loop()
        )

    async def _dispatch_batch(self, data):
        return simplejson.dumps(await self.batch_executor.handle_batch(data))

    @staticmethod
    def _get_method_args_kwargs(request_params):
        """
        Распознает args и kwargs в зависмости от типа данных
        :param request_params:
        :return:
        """
        args = []
        kwargs = {}
        if isinstance(request_params, list):
            args = request_params
        if isinstance(request_params, dict):
            kwargs = request_params
        return args, kwargs

    def _get_invalid_response(self, request_dict):
        """
        Проверяет запрос на невалидность
        :param request_dict:
        :return:
        """
        request_id = request_dict.get('id')

        if not validate_request(request_dict):
            # При невалидном запросе все же есть шанс найти id запроса и вернуть
            # с ошибкой
            possible_id = None
            if isinstance(request_dict, dict):
                possible_id = request_dict.get('id')
            return invalid_request(id=possible_id)
        req_method = request_dict['method']
        if req_method not in self.method_router:
            return method_not_found(id=request_id)

        method = self.method_router[req_method]

        args, kwargs = self._get_method_args_kwargs(request_dict.get('params'))

        if is_invalid_params(method, *args, **kwargs):
            return invalid_params(id=request_id)

    @staticmethod
    def _handle_exception(ex, request_id):
        """
        Позволяет обрабатывать пользовательские исключения в формате jsonrpc 2.0
        :param ex:
        :param request_id:
        :return:
        """
        if isinstance(ex, RpcResponseException):
            return Response.make_error(
                id=request_id,
                err_code=ex.code,
                err_msg=ex.msg,
                err_data={
                    'traceback': traceback.format_exc(),
                    'other_data': ex.data
                }
            )
        else:
            return internal_error(
                id=request_id,
                err_data={
                    'traceback': traceback.format_exc(),
                    'verbose': str(ex)
                }
            )

    @staticmethod
    def _is_notification(request_dict):
        """
        Без проверки на dict не определить уведомление это или нет, так как
        проверять нужно до валидации, но на этот момент неизвестно что вообще
        пришло
        :param request_dict:
        :return:
        """
        return isinstance(request_dict, dict) and request_dict.get('id') is None

    async def _prepare_response(self, request_dict):
        """
        Тут выполняется подготовка ответа
        Сперва происходит валидация
        Потом выделяются аргументы ф-ции
        Отлавливаются исключения
        Возвращается ответ
        :param request_dict:
        :return:
        """
        is_notification = self._is_notification(request_dict)

        invalid_response = self._get_invalid_response(request_dict)
        if invalid_response:
            return None if is_notification else invalid_response

        request_id = request_dict.get('id')
        method = self.method_router[request_dict['method']]
        args, kwargs = self._get_method_args_kwargs(request_dict.get('params'))

        try:
            method_result = await method(*args, **kwargs) if asyncio.iscoroutinefunction(method) \
                else method(*args, **kwargs)
        except Exception as ex:
            result = self._handle_exception(ex, request_id)
        else:
            result = Response.make_result(id=request_id, result=method_result)
        finally:
            return None if is_notification else result

    def _prepare_response_sync(self, request_dict):
        """
        Синхронная версия ф-ции подготовки ответа, для возможности выполнять
        CPU bound задачи
        Это другая точка входа, логика тут может дублироваться
        :param request_dict:
        :return:
        """
        is_notification = self._is_notification(request_dict)

        invalid_response = self._get_invalid_response(request_dict)
        if invalid_response:
            return None if is_notification else invalid_response

        request_id = request_dict.get('id')
        method = self.method_router[request_dict['method']]
        args, kwargs = self._get_method_args_kwargs(request_dict.get('params'))

        try:
            method_result = method(*args, **kwargs)
        except Exception as ex:
            result = self._handle_exception(ex, request_id)
        else:
            result = Response.make_result(id=request_id, result=method_result)
        finally:
            return None if is_notification else result

    async def _process_request(self, request_dict):
        """
        Метод подготавливает ответ, возможен CPU bound запрос
        :param request_dict:
        :return:
        """
        if isinstance(request_dict, dict):
            method_type = self.method_types.get(request_dict.get('method'))
            if method_type in MARKS_CPU_BOUND:
                return simplejson.dumps(
                    await self.thread_executor.collect_one_result(request_dict)
                )
        return simplejson.dumps(await self._prepare_response(request_dict))

    @staticmethod
    def _check_method(method, method_type):
        """
        Проверяет ф-ции при регистрации, CPU bound ф-ции не должны быть асинхронными
        :param method:
        :param method_type:
        :return:
        """
        if asyncio.iscoroutinefunction(method) and method_type in MARKS_CPU_BOUND:
            raise RpcException('CPU bound method can\'t be async')

    def _register_method(self, method, method_type, name=None):
        """
        Регистрация метода по имени, так же регистрируется тип, чтобы позже
        понять как его вызывать
        :param method:
        :param method_type:
        :param name:
        :return:
        """
        self._check_method(method, method_type)

        method_name = name or method.__name__

        self.method_router[method_name] = method
        self.method_types[method_name] = method_type

    def method(self, name=None):
        """
        Этот декоратор используется клиентским кодом для регистрации асинхронной
        ф-ции
        :param name:
        :return:
        """
        def decorator(method):
            self._register_method(method, MARK_ASYNC, name)
        return decorator

    def method_thread(self, name=None):
        """
        Этот декоратор используется клиентским кодом для регистрации CPU bound
        ф-ции на основе потоков
        :param name:
        :return:
        """
        def decorator(method):
            self._register_method(method, MARK_THREAD, name)
        return decorator

    async def dispatch(self, data_str):
        """
        Решает как обрабатывать запрос, бывают одиночные и массовые, так же
        асинхронные и CPU bound, CPU bound внутри массового запроса обрабатывает
        batch_executor
        :param data_str:
        :return:
        """
        try:
            decoded_data = simplejson.loads(data_str)
        except JSONDecodeError:
            return simplejson.dumps(parse_error())
        else:
            # batch запрос
            if isinstance(decoded_data, list):
                return await self._dispatch_batch(decoded_data)
            else:
                return await self._process_request(decoded_data)
