from inspect import signature


def is_invalid_params(func, *args, **kwargs):
    """
    Проверяет подходят ли параметры для сигнатуры ф-ции
    Способ варварский, но в данном контексте безопасный, так же избавляет от логических ошибок
    :param func:
    :param args:
    :param kwargs:
    :return:
    """
    exec(f'def f{str(signature(func))}: pass')

    try:
        locals()['f'](*args, **kwargs)
    except TypeError:
        return True
    else:
        return False
