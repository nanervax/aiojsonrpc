import aiohttp
from aiohttp import web, ClientError

from aiojsonrpc.constants import REQUEST_TIMEOUT
from aiojsonrpc.exceptions import RpcRequestException
import aiojsonrpc.request as request


def as_aiohttp_router(endpoint, app):
    """
    Позволяет представить endpoint как router в aiohttp
    :param app:
    :param endpoint:
    :return:
    """

    async def aiohttp_dispatcher(request):
        # не jsonresponse, так как dispatch возвращает уже строку
        return web.Response(
            text=await endpoint.dispatch(await request.text()),
            content_type='application/json'
        )

    app.add_routes([
        web.post(f'/{endpoint.namespace}/{endpoint.version}/', aiohttp_dispatcher)
    ])


class BackendRequest:
    """
    Клиентские запросы с помощью aiohttp
    aiohttp не рекомендует на каждый запрос создавать сессию
    """
    session = None

    @classmethod
    def get_session(cls):
        if not cls.session:
            cls.session = aiohttp.ClientSession(
                timeout=aiohttp.ClientTimeout(total=REQUEST_TIMEOUT)
            )
        return cls.session

    @classmethod
    async def do_request(cls, url, req):
        try:
            async with cls.get_session().post(url, data=req) as response:
                return await response.text()
        except ClientError:
            raise RpcRequestException('Request error')


# Классы - запросы со внедренными зависимостями aiohttp
class Request(request.Request):
    backend_request_cls = BackendRequest


class BatchRequest(request.BatchRequest):
    backend_request_cls = BackendRequest
