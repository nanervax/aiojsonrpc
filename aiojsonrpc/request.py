from abc import abstractmethod
from uuid import uuid4

import simplejson
from simplejson import JSONDecodeError

from .response import Response
from .schema import validate_response
from .defined_errors import response_parse_error, invalid_response, invalid_response_batch


class BaseRequest:
    """
    Стандартный JSON RPC 2.0 запрос
    """

    backend_request_cls = None

    @classmethod
    def _make_request(cls, method, params=None, id=None):
        """
        Метод обеспечивает стандартную структуру ответа
        :param method:
        :param params:
        :param id:
        :return:
        """
        request = {
            'jsonrpc': '2.0',
            'method': method
        }

        if params:
            request['params'] = params
        if id:
            request['id'] = id
        return request

    @classmethod
    def _prepare_request(cls, request):
        return simplejson.dumps(request)

    @classmethod
    def _prepare_id(cls, id):
        return id if id is not None else str(uuid4())

    @classmethod
    async def _do_request(cls, path, request):
        """
        Отлавливаются ошибки парсинга JSON и неверной структуры ответа
        Так же клиент получит ошибку, если система не смогла сделать запрос
        Должна быть реализация класса, который делает запрос
        :param path:
        :param request:
        :return:
        """
        try:
            do_request_method = cls.backend_request_cls.do_request
        except AttributeError:
            raise NotImplementedError

        try:
            decoded_response = simplejson.loads(
                await do_request_method(path, cls._prepare_request(request))
            )
        except JSONDecodeError:
            return Response(**response_parse_error())
        else:
            return cls._handle_response(request, decoded_response)

    @classmethod
    @abstractmethod
    def _handle_response(cls, request, response):
        raise NotImplemented


class Request(BaseRequest):
    @classmethod
    async def notice(cls, path, method, params=None):
        return await cls._do_request(path, cls._make_request(method, params))

    @classmethod
    async def request(cls, path, method, params=None, id=None):
        """
        Протокол разрешает использовать null в id
        Но по идее он нужен только в ответе, когда не удалось спарсить структуру
        запроса с корректным id
        :param path:
        :param method:
        :param id:
        :param param_args:
        :param param_kwargs:
        :return:
        """
        id = cls._prepare_id(id)
        return await cls._do_request(path, cls._make_request(method, params, id))

    @classmethod
    def _handle_response(cls, request, response):
        # На уведомления нужно возращать None
        if not request.get('id'):
            return None
        if not validate_response(response):
            return Response(invalid_response())
        else:
            return Response(response)


class BatchRequest(BaseRequest):
    """
    Позволяет делать массовые запросы
    Работает по принципу сбора запросов и массовой отправки
    """
    def __init__(self, path):
        self.batch = []
        self.path = path

    def add_request(self, method, params=None, id=None):
        """
        Добавляет запрос в стркутуру для последующей отправки
        :param method:
        :param params:
        :param id:
        :return:
        """
        id = self._prepare_id(id)
        self.batch.append(self._make_request(method, params, id))
        return self

    def add_notice(self, method, params=None):
        """
        Добавляет уведомление для последующей отправки
        :param method:
        :param params:
        :return:
        """
        self.batch.append(self._make_request(method, params))
        return self

    def send(self):
        """
        Отправляет пачку накопленных запросов
        Отправленное не очищается
        :return:
        """
        return self._do_request(self.path, self.batch)

    @classmethod
    def _handle_response(cls, request, responses):
        if not isinstance(responses, (list, tuple)):
            return Response(**invalid_response_batch())
        else:
            cleaned = []
            for response in responses:
                if not validate_response(response):
                    cleaned.append(Response(invalid_response()))
                    continue
                cleaned.append(Response(response))
            return cleaned
