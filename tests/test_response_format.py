from pytest import fixture

from aiojsonrpc.schema import validate_response

correct_responses = [
    {
        'jsonrpc': '2.0',
        'error': {
            'code': -32700,
            'message': 'Parse error',
        },
        'id': None
    },
    {
        'jsonrpc': '2.0',
        'error': {
            'code': -32700,
            'message': 'Parse error',
            'data': {'foo': 'bar'}
        },
        'id': 324
    },
    {
        'jsonrpc': '2.0',
        'error': {
            'code': -32700,
            'message': 'Parse error',
            'data': 'baz'
        },
        'id': '91c4dea8-c116-4e3e-9d93-f07ed1e8414c'
    },
    {
        'jsonrpc': '2.0',
        'result': {},
        'id': '91c4dea8-c116-4e3e-9d93-f07ed1e8414c'
    },
    {
        'jsonrpc': '2.0',
        'result': {'props': {'foo': 'bar'}},
        'id': 123
    },
    {
        'jsonrpc': '2.0',
        'result': 'bar',
        'id': 123
    },
    {
        'jsonrpc': '2.0',
        'result': 34,
        'id': 123
    }
]

incorrect_responses = [
    {
        'jsonrpc': '2.0',
        'error': {
            'code': -32700,
            'message': 'Parse error',
            'data': 'baz',
            'foo': 'bar'
        },
        'id': '91c4dea8-c116-4e3e-9d93-f07ed1e8414c'
    },
    {
        'jsonrpc': '2.0',
        'error': {
            'code': -32700,
            'message': 'Parse error',
        },
        'id': None,
        'bas': 'fsad'
    },
    {
        'jsonrpc': '3.0',
        'error': {
            'code': -32700,
            'message': 'Parse error',
        },
        'id': None
    },
    {
        'jsonrpc': '2.0',
        'id': None
    },
    {
        'jsonrpc': '2.0',
        'error': {
            'code': '234',
            'message': 'Parse error',
        },
        'id': None
    },
    {
        'jsonrpc': '2.0',
        'error': {
            'code': 234
        },
        'id': None
    },
    {
        'jsonrpc': '2.0',
        'error': {
            'code': 234,
            'message': {'foo': 'bar'}
        },
        'id': None
    },
    {
        'jsonrpc': '2.0',
        'error': {
            'message': 'ga'
        },
        'id': None
    },
    {},
    {
        'jsonrpc': '2.0',
        'result': {},
        'error': {
            'code': -32700,
            'message': 'Parse error',
        },
        'id': '91c4dea8-c116-4e3e-9d93-f07ed1e8414c'
    },
    {
        'jsonrpc': '2.0',
        'error': {
            'code': -32700,
            'message': 'Parse error',
        },
        'id': {'foo': 'bar'}
    },
    {
        'jsonrpc': '2.0',
        'error': {
            'code': -32700,
            'message': 'Parse error',
        }
    },
    {
        'jsonrpc': '2.0',
    },
]


@fixture(scope='module', params=correct_responses)
def correct_response(request):
    return request.param


@fixture(scope='module', params=incorrect_responses)
def incorrect_response(request):
    return request.param


def test_correct(correct_response):
    assert validate_response(correct_response)


def test_incorrect(incorrect_response):
    assert not validate_response(incorrect_response)
