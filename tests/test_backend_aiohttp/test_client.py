from aiohttp import web

from pytest import fixture

from aiojsonrpc import Endpoint
from aiojsonrpc.backend.aiohttp_backend import as_aiohttp_router, BatchRequest, Request, \
    BackendRequest

URL = '/jsonrpc/v1/'


async def echo_kwargs(**kwargs):
    return kwargs


async def echo_args(*args):
    return args


async def sum_nums(foo, bar, *args):
    res = foo + bar
    res += sum(args)
    return res


async def mul(bar, baz=10):
    return bar * baz


@fixture
def client(loop, aiohttp_client):
    app = web.Application()

    endpoint = Endpoint(namespace='jsonrpc', version='v1')
    endpoint.method('echo_kwargs')(echo_kwargs)
    endpoint.method('echo_args')(echo_args)
    endpoint.method('sum_nums')(sum_nums)
    endpoint.method('mul')(mul)

    as_aiohttp_router(endpoint, app)
    return loop.run_until_complete(aiohttp_client(app))


async def test_request(monkeypatch, client):
    monkeypatch.setattr(BackendRequest, 'session', client)

    resp = await Request.notice(URL, 'echo_kwargs', {'foo': 'bar'})
    assert resp is None

    resp = await Request.request(URL, 'echo_kwargs', {'foo': 'bar'}, 5)
    assert resp.id == 5
    assert resp.result == {'foo': 'bar'}

    resp = await Request.request(URL, 'echo_kwargs', {'foo': 'bar'})
    assert bool(resp.id)
    assert resp.result == {'foo': 'bar'}

    resp = await Request.request(URL, 'echo_args', [10, 20, 30])
    assert resp.result == [10, 20, 30]

    resp = await Request.request(URL, 'sum_nums', [10, 20, 30])
    assert resp.result == 60

    resp = await Request.request(URL, 'sum_nums', [10, 20])
    assert resp.result == 30

    resp = await Request.request(URL, 'mul', [10, 20])
    assert resp.result == 200

    resp = await Request.request(URL, 'mul', [10])
    assert resp.result == 100


async def test_batch_request(monkeypatch, client):
    monkeypatch.setattr(BackendRequest, 'session', client)

    resp = await BatchRequest(URL) \
        .add_request('echo_kwargs', {'baz': 'spam'}, 34) \
        .add_notice('echo_kwargs', {'baz': 'spam'}) \
        .add_request('echo_kwargs', {'baz1': 'spam1'}, 44) \
        .send()

    assert len(resp) == 2

    responses_dict = {one_of_batch.id: one_of_batch for one_of_batch in resp}

    assert responses_dict[34].result == {'baz': 'spam'}
    assert responses_dict[44].result == {'baz1': 'spam1'}
