import asyncio

import datetime
import threading
import time

import simplejson

from aiojsonrpc.exceptions import RpcResponseException
from aiojsonrpc.endpoint import Endpoint
from aiojsonrpc.defined_errors import *


def req_handler(foo, bar):
    return foo * bar


async def a_req_handler(foo, bar):
    return foo + bar


def broken_req_handler(foo, bar):
    raise ValueError()


MANUAL_ERR_CODE = 111
MANUAL_ERR_MSG = 'baz'
MANUAL_ERR_DATA = {'111': '222'}


def manual_broken_req_handler():
    raise RpcResponseException(code=MANUAL_ERR_CODE, msg=MANUAL_ERR_MSG, data=MANUAL_ERR_DATA)


async def test_incorrect_requests():
    endpoint = Endpoint()
    endpoint.method('foo')(req_handler)
    endpoint.method('spam')(broken_req_handler)
    endpoint.method('zee')(manual_broken_req_handler)

    response = await endpoint.dispatch("asd'")
    assert simplejson.loads(response)['error']['code'] == PARSE_ERROR

    response = await endpoint.dispatch(simplejson.dumps({'jsonrcc': '3', 'id': 1}))
    assert simplejson.loads(response)['error']['code'] == INVALID_REQUEST
    assert simplejson.loads(response)['id'] == 1

    response = await endpoint.dispatch(simplejson.dumps({
        'jsonrpc': '2.0',
        'method': 'bar',
        'id': 1
    }))
    assert simplejson.loads(response)['error']['code'] == METHOD_NOT_FOUND
    assert simplejson.loads(response)['id'] == 1

    response = await endpoint.dispatch(simplejson.dumps({
        'jsonrpc': '2.0',
        'method': 'spam',
        'params': [1, 2],
        'id': 1
    }))
    assert simplejson.loads(response)['error']['code'] == INTERNAL_ERROR
    assert simplejson.loads(response)['id'] == 1

    response = await endpoint.dispatch(simplejson.dumps({
        'jsonrpc': '2.0',
        'method': 'foo',
        'params': [1, 2, 3],
        'id': 1
    }))
    assert simplejson.loads(response)['error']['code'] == INVALID_PARAMS
    assert simplejson.loads(response)['id'] == 1

    response = await endpoint.dispatch(simplejson.dumps({
        'jsonrpc': '2.0',
        'method': 'foo',
        'params': {'gdsoo': 2, 'bar': 3},
        'id': 1
    }))
    assert simplejson.loads(response)['error']['code'] == INVALID_PARAMS
    assert simplejson.loads(response)['id'] == 1

    response = await endpoint.dispatch(simplejson.dumps({
        'jsonrpc': '2.0',
        'method': 'zee',
        'id': 1
    }))
    assert simplejson.loads(response)['error']['code'] == MANUAL_ERR_CODE
    assert simplejson.loads(response)['error']['message'] == MANUAL_ERR_MSG
    assert simplejson.loads(response)['error']['data']['other_data'] == MANUAL_ERR_DATA
    assert simplejson.loads(response)['id'] == 1

    # Ошибочные уведомления
    response = await endpoint.dispatch(
        simplejson.dumps({'jsonrcc': '3'}))
    assert simplejson.loads(response) is None

    response = await endpoint.dispatch(simplejson.dumps({
        'jsonrpc': '2.0',
        'method': 'bar',
    }))
    assert simplejson.loads(response) is None

    response = await endpoint.dispatch(simplejson.dumps({
        'jsonrpc': '2.0',
        'method': 'spam',
        'params': [1, 2],
    }))
    assert simplejson.loads(response) is None

    response = await endpoint.dispatch(simplejson.dumps({
        'jsonrpc': '2.0',
        'method': 'foo',
        'params': [1, 2, 3],
    }))
    assert simplejson.loads(response) is None

    response = await endpoint.dispatch(simplejson.dumps({
        'jsonrpc': '2.0',
        'method': 'foo',
        'params': {'gdsoo': 2, 'bar': 3},
    }))
    assert simplejson.loads(response) is None

    response = await endpoint.dispatch(simplejson.dumps({
        'jsonrpc': '2.0',
        'method': 'zee',
    }))
    assert simplejson.loads(response) is None


async def test_correct_requests():
    endpoint = Endpoint()
    endpoint.method('foo')(req_handler)
    endpoint.method('a_foo')(a_req_handler)

    # Уведомление
    response = await endpoint.dispatch(simplejson.dumps({
        'jsonrpc': '2.0',
        'method': 'foo',
        'params': [56, 2]
    }))

    assert simplejson.loads(response) is None

    response = await endpoint.dispatch(simplejson.dumps({
        'jsonrpc': '2.0',
        'method': 'foo',
        'params': [56, 2],
        'id': 3
    }))

    assert simplejson.loads(response)['result'] == 112
    assert simplejson.loads(response)['id'] == 3

    response = await endpoint.dispatch(simplejson.dumps({
        'jsonrpc': '2.0',
        'method': 'foo',
        'params': {'foo': 56, 'bar': 2},
        'id': 3
    }))

    assert simplejson.loads(response)['result'] == 112
    assert simplejson.loads(response)['id'] == 3

    response = await endpoint.dispatch(simplejson.dumps({
        'jsonrpc': '2.0',
        'method': 'a_foo',
        'params': {'foo': 56, 'bar': 2},
        'id': 13
    }))

    assert simplejson.loads(response)['result'] == 58
    assert simplejson.loads(response)['id'] == 13


def req_handler_for_thread():
    return threading.current_thread().name


async def test_correct_requests_threaded():
    endpoint = Endpoint()
    endpoint.method_thread('foo')(req_handler_for_thread)

    response = await endpoint.dispatch(simplejson.dumps({
        'jsonrpc': '2.0',
        'method': 'foo',
        'id': 3
    }))

    assert simplejson.loads(response)['result'] != 'MainThread'


async def req_handler_with_delay():
    await asyncio.sleep(2)


async def test_notify_execution():
    endpoint = Endpoint()
    endpoint.method('foo')(req_handler_with_delay)

    start = datetime.datetime.utcnow()

    await endpoint.dispatch(simplejson.dumps({
        'jsonrpc': '2.0',
        'method': 'foo'
    }))

    duration_sec = (datetime.datetime.utcnow() - start).total_seconds()

    assert duration_sec > 2


async def req_handler_1(foo, bar):
    return foo * bar


async def req_handler_2(foo, bar):
    return foo + bar


async def req_handler_3(foo, bar):
    return foo + bar


async def test_batch_requests():
    endpoint = Endpoint()
    endpoint.method('foo')(req_handler_1)
    endpoint.method('bar')(req_handler_2)
    endpoint.method('spam')(req_handler_3)

    response = await endpoint.dispatch(simplejson.dumps([
        {
            'method': 'foo',
            'params': [56, 2],
            'id': 12
        },
        {
            'jsonrpc': '2.0',
            'method': 'bar',
            'params': [56, 2],
            'id': 312
        },
        {
            'jsonrpc': '2.0',
            'method': 'spam',
            'params': [111, 2, 3],
            'id': 112
        },
        {
            'jsonrpc': '2.0',
            'method': 'foo',
            'params': [111, 2]
        },
    ]))

    assert len(simplejson.loads(response)) == 3

    assert simplejson.loads(response)[0]['error']['code'] == INVALID_REQUEST
    assert simplejson.loads(response)[0]['id'] == 12

    assert simplejson.loads(response)[2]['error']['code'] == INVALID_PARAMS
    assert simplejson.loads(response)[2]['id'] == 112


async def req_handler_4(foo, bar):
    await asyncio.sleep(1)
    return foo * bar


async def req_handler_5(foo, bar):
    await asyncio.sleep(1)
    return foo + bar


async def req_handler_6(foo, bar):
    await asyncio.sleep(1)
    return foo + bar


async def test_batch_requests_with_duration():
    endpoint = Endpoint()
    endpoint.method('foo')(req_handler_4)
    endpoint.method('bar')(req_handler_5)
    endpoint.method('spam')(req_handler_6)

    start = datetime.datetime.utcnow()

    response = await endpoint.dispatch(simplejson.dumps([
        {
            'jsonrpc': '2.0',
            'method': 'foo',
            'params': [56, 2],
            'id': 12
        },
        {
            'jsonrpc': '2.0',
            'method': 'bar',
            'params': [56, 2],
            'id': 312
        },
        {
            'jsonrpc': '2.0',
            'method': 'spam',
            'params': [111, 2],
            'id': 112
        },
        {
            'jsonrpc': '2.0',
            'method': 'foo',
            'params': [111, 2]
        },
    ]))

    duration_sec = (datetime.datetime.utcnow() - start).total_seconds()

    assert simplejson.loads(response)[0]['result'] == 112
    assert simplejson.loads(response)[0]['id'] == 12

    assert simplejson.loads(response)[1]['result'] == 58
    assert simplejson.loads(response)[1]['id'] == 312

    assert simplejson.loads(response)[2]['result'] == 113
    assert simplejson.loads(response)[2]['id'] == 112

    assert duration_sec < 2


def thread_handler_1():
    time.sleep(2)
    return threading.current_thread().name


def thread_handler_2(foo, bar):
    return foo + bar


async def async_handler_3(foo, bar):
    await asyncio.sleep(2)
    return foo + bar


async def test_batch_requests_with_cpu_bound():
    endpoint = Endpoint()
    endpoint.method_thread('thread1')(thread_handler_1)
    endpoint.method_thread('thread2')(thread_handler_2)
    endpoint.method('async')(async_handler_3)

    start = datetime.datetime.utcnow()

    response = await endpoint.dispatch(simplejson.dumps([
        {
            'method': 'thread1',
            'id': 12
        },
        {
            'jsonrpc': '2.0',
            'method': 'thread1',
            'id': 312
        },
        {
            'jsonrpc': '2.0',
            'method': 'thread2',
            'params': [56, 2],
            'id': 312
        },
        {
            'jsonrpc': '2.0',
            'method': 'async',
            'params': [111, 2]
        },
    ]))

    duration_sec = (datetime.datetime.utcnow() - start).total_seconds()
    assert duration_sec < 4

    responses_dict = {one_of_batch['id']: one_of_batch for one_of_batch in simplejson.loads(response)}

    assert responses_dict[312]['result'] != 'MainThread'
