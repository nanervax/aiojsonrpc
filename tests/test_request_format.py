from pytest import fixture

from aiojsonrpc.schema import validate_request

correct_requests = [
    {
        'jsonrpc': '2.0',
        'method': 'bar'
    },
    {
        'jsonrpc': '2.0',
        'method': 'bar',
        'params': ['foo', 'bar']
    },
    {
        'jsonrpc': '2.0',
        'method': 'bar',
        'params': {'foo': 'bar', 'spam': 'baz'}
    },
    {
        'jsonrpc': '2.0',
        'method': 'bar',
        'params': {'foo': 'bar', 'spam': 'baz'},
        'id': None
    },
    {
        'jsonrpc': '2.0',
        'method': 'bar',
        'params': {'foo': 'bar', 'spam': 'baz'},
        'id': 1
    },
    {
        'jsonrpc': '2.0',
        'method': 'bar',
        'params': {'foo': 'bar', 'spam': 'baz'},
        'id': '91c4dea8-c116-4e3e-9d93-f07ed1e8414c'
    }
]

incorrect_requests = [
    {
        'jsonrpc': '2.0',
        'method': 'bar',
        'basx': 'asd'
    },
    {
        'jsonrpc': '3.0',
        'method': 'bar',
        'params': {'foo': 'bar', 'spam': 'baz'},
        'id': '91c4dea8-c116-4e3e-9d93-f07ed1e8414c'
    },
    {
        'jsonrpc': '2.0',
        'method': ['foo', 'bar'],
        'params': {'foo': 'bar', 'spam': 'baz'},
        'id': '91c4dea8-c116-4e3e-9d93-f07ed1e8414c'
    },
    {
        'jsonrpc': '2.0',
        'params': {'foo': 'bar', 'spam': 'baz'},
        'id': '91c4dea8-c116-4e3e-9d93-f07ed1e8414c'
    },
    {
        'jsonrpc': '2.0',
        'method': 'bar',
        'params': 'baz',
        'id': '91c4dea8-c116-4e3e-9d93-f07ed1e8414c'
    },
    {
        'jsonrpc': '2.0',
        'method': 'bar',
        'params': None,
        'id': '91c4dea8-c116-4e3e-9d93-f07ed1e8414c'
    },
    {}
]


@fixture(scope='module', params=correct_requests)
def correct_request(request):
    return request.param


@fixture(scope='module', params=incorrect_requests)
def incorrect_request(request):
    return request.param


def test_correct(correct_request):
    assert validate_request(correct_request)


def test_incorrect(incorrect_request):
    assert not validate_request(incorrect_request)
