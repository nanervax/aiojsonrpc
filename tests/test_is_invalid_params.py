from aiojsonrpc.helper import is_invalid_params


def check_func(func, *args, **kwargs):
    is_invalid_params_res = is_invalid_params(func, *args, **kwargs)

    try:
        func(*args, **kwargs)
    except TypeError as e:
        return True == is_invalid_params_res
    else:
        return False == is_invalid_params_res


def func_1():
    pass


def func_2(bar):
    pass


def func_3(bar=0):
    pass


def func_4(foo, bar=0):
    pass


def func_5(*args):
    pass


def func_6(foo, *args):
    pass


def func_7(**kwargs):
    pass


def func_8(foo, bar, *args, **kwargs):
    pass


def test_invalid_params():
    assert check_func(func_1)
    assert check_func(func_1, 3)
    assert check_func(func_1, 3, 34)
    assert check_func(func_1, foo=34)

    assert check_func(func_2)
    assert check_func(func_2, 3)
    assert check_func(func_2, 3, 34)
    assert check_func(func_2, foo=34)
    assert check_func(func_2, bar=34)

    assert check_func(func_3)
    assert check_func(func_3, 3)
    assert check_func(func_3, 3, 34)
    assert check_func(func_3, foo=34)
    assert check_func(func_3, bar=34)

    assert check_func(func_4)
    assert check_func(func_4, 3)
    assert check_func(func_4, 3, 34)
    assert check_func(func_4, foo=34)
    assert check_func(func_4, bar=34)
    assert check_func(func_4, bar=34, baz=34)
    assert check_func(func_4, 1, 2, 3)

    assert check_func(func_5, foo=3)
    assert check_func(func_5, 1)
    assert check_func(func_5, 1, 2)
    assert check_func(func_5, 1, 3, 4)

    assert check_func(func_6, 1, 2, foo=3)
    assert check_func(func_6, 1)
    assert check_func(func_6)
    assert check_func(func_6, foo=45)
    assert check_func(func_6, 1, 2)
    assert check_func(func_6, 1, 3, 4)

    assert check_func(func_7, 1)
    assert check_func(func_7, 1, 2)
    assert check_func(func_7)
    assert check_func(func_7, foo=45)
    assert check_func(func_7, foo=34, bar=345)
    assert check_func(func_7, 123, foo=34, bar=345)

    assert check_func(func_8, 1)
    assert check_func(func_8, 1, 2)
    assert check_func(func_8)
    assert check_func(func_8, foo=45)
    assert check_func(func_8, foo=34, bar=345)
    assert check_func(func_8, 123, foo=34, bar=345)
