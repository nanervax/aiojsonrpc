## Реализация протокола jsonrpc 2.0

### Возможности
https://www.jsonrpc.org/specification  
Предоставляет реализацию json-rpc 2.0 в асинхронном коде  
В пакете реализован сам протокол + опционально aiohttp транспорт  
Для использования транспорта нужно ставить соответствующие зависимости  
При регистрировании ф-ций можно использовать \*args, \*\*kwargs  
```python
@endpoint.method_thread()
def sum_nums(foo, bar, *args):
    res = foo + bar
    res += sum(args)
    return res
```
 
### Тесты
Все внутри **tests**, там так же есть директории с тестами опциональных бекендов

### Примеры использования
Запуск сервера:  
```python
import asyncio
import time

from aiohttp import web

from aiojsonrpc import Endpoint
from aiojsonrpc.backend.aiohttp_backend import as_aiohttp_router

app = web.Application()
endpoint = Endpoint()
as_aiohttp_router(endpoint, app)


@endpoint.method_thread()
def foo(a, b):
    time.sleep(2)
    return {'result': a * b}


@endpoint.method()
async def bar(a, b):
    await asyncio.sleep(2)
    return {'result': a + b}


web.run_app(app)
```
 - @endpoint.method() - обычное асинхронное выполнение  
 - @endpoint.method_thread() - будет выполняться в отдельном потоке, результат будет ждаться асинхронно  

Использование клиента:  
```python
resp = await Request.notice(URL, 'echo_kwargs', {'foo': 'bar'})
resp = await Request.request(URL, 'echo_kwargs', {'foo': 'bar'}, 5)
resp = await Request.request(URL, 'echo_kwargs', {'foo': 'bar'})
resp = await Request.request(URL, 'echo_args', [10, 20, 30])
resp = await Request.request(URL, 'sum_nums', [10, 20, 30])
resp = await Request.request(URL, 'sum_nums', [10, 20])
resp = await Request.request(URL, 'mul', [10, 20])
resp = await Request.request(URL, 'mul', [10])
```
request последним параметром принимает id, если не передать, будет сгенерен uuid4  
И массовые запросы:
```python
resp = await BatchRequest(URL) \
    .add_request('echo_kwargs', {'baz': 'spam'}, 34) \
    .add_notice('echo_kwargs', {'baz': 'spam'}) \
    .add_request('echo_kwargs', {'baz1': 'spam1'}, 44) \
    .send()
```
В данной версии ответ крайне простой - отражает структуру по протоколу, ничего о метаинформации транспорта не знает
  